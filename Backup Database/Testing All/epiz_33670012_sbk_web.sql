-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql110.byetcluster.com
-- Waktu pembuatan: 01 Mar 2023 pada 04.23
-- Versi server: 10.3.27-MariaDB
-- Versi PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_33670012_sbk_web`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_invoices`
--

CREATE TABLE `detail_invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idPesanan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idProduk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaProduk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kemasan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoices`
--

CREATE TABLE `invoices` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idMember` bigint(20) UNSIGNED NOT NULL,
  `tanggalPesanan` date NOT NULL DEFAULT curdate(),
  `statusPesanan` enum('Proses','Selesai') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Proses',
  `totalPembayaran` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `invoices`
--

INSERT INTO `invoices` (`id`, `idMember`, `tanggalPesanan`, `statusPesanan`, `totalPembayaran`, `created_at`, `updated_at`) VALUES
('SBK/BJB-2303001', 1, '2023-03-01', 'Proses', 0, '2023-03-01 14:30:47', '2023-03-01 14:30:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemen_members`
--

CREATE TABLE `manajemen_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaMember` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaToko` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamatToko` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomorHP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `manajemen_members`
--

INSERT INTO `manajemen_members` (`id`, `namaMember`, `namaToko`, `alamatToko`, `nomorHP`, `created_at`, `updated_at`) VALUES
(1, 'Akhmad Fauzi', 'BlackBoY Store', 'Jl. Palam', '+6281347890520', '2023-03-01 14:29:38', '2023-03-01 14:29:38'),
(2, 'Yuni Novita Br Sihaloho', 'TK. Mbak Yunnnn', 'Tegallll', '+6285325699630', '2023-03-01 14:30:31', '2023-03-01 14:30:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemen_stoks`
--

CREATE TABLE `manajemen_stoks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namaProduk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kemasan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlahStok` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `manajemen_stoks`
--

INSERT INTO `manajemen_stoks` (`id`, `namaProduk`, `kemasan`, `jumlahStok`, `satuan`, `created_at`, `updated_at`) VALUES
(1, 'Reaktif 490 SL', '250 ML', 0, 'L', NULL, '2023-02-28 03:13:43'),
(2, 'Reaktif 490 SL', '500 ML', 0, 'L', NULL, NULL),
(3, 'Reaktif 490 SL', '1 L', 0, 'L', NULL, NULL),
(4, 'Reaktif 490 SL', '5 L', 0, 'L', NULL, NULL),
(5, 'Reaktif 490 SL', '20 L', 0, 'L', NULL, NULL),
(6, 'Ralang 480 SL', '1 L', 0, 'L', NULL, NULL),
(7, 'Ralang 480 SL', '5 L', 0, 'L', NULL, NULL),
(8, 'Ralang 480 SL', '20 L', 0, 'L', NULL, NULL),
(9, 'Ghoxone 138 SL', '250 ML', 0, 'L', NULL, NULL),
(10, 'Ghoxone 138 SL', '500 ML', 0, 'L', NULL, NULL),
(11, 'Ghoxone 138 SL', '1 L', 0, 'L', NULL, NULL),
(12, 'Ghoxone 138 SL', '5 L', 0, 'L', NULL, NULL),
(13, 'Ghoxone 138 SL', '20 L', 0, 'L', NULL, NULL),
(14, 'Bround Up 240 SL', '250 ML', 0, 'L', NULL, NULL),
(15, 'Bround Up 240 SL', '500 ML', 0, 'L', NULL, NULL),
(16, 'Bround Up 240 SL', '1 L', 0, 'L', NULL, NULL),
(17, 'Bround Up 240 SL', '5 L', 0, 'L', NULL, NULL),
(18, 'Bround Up 240 SL', '20 L', 0, 'L', NULL, NULL),
(19, 'Kontaxone 310 SL', '500 ML', 0, 'L', NULL, NULL),
(20, 'Kontaxone 310 SL', '1 L', 0, 'L', NULL, NULL),
(21, 'Kontaxone 310 SL', '5 L', 0, 'L', NULL, NULL),
(22, 'Kontaxone 310 SL', '20 L', 0, 'L', NULL, NULL),
(23, 'Rolixone 276 SL', '1 L', 0, 'L', NULL, NULL),
(24, 'Rolixone 276 SL', '5 L', 0, 'L', NULL, NULL),
(25, 'Rolixone 276 SL', '20 L', 0, 'L', NULL, NULL),
(26, 'Primaxone 276 SL', '500 ML', 0, 'L', NULL, NULL),
(27, 'Primaxone 276 SL', '1 L', 0, 'L', NULL, NULL),
(28, 'Primaxone 276 SL', '5 L', 0, 'L', NULL, NULL),
(29, 'Primaxone 276 SL', '20 L', 0, 'L', NULL, NULL),
(30, 'Mantapxone 135 SL', '1 L', 0, 'L', NULL, NULL),
(31, 'Mantapxone 135 SL', '5 L', 0, 'L', NULL, NULL),
(32, 'Mantapxone 135 SL', '20 L', 0, 'L', NULL, NULL),
(33, 'Roll Up 480 SL', '500 ML', 0, 'L', NULL, NULL),
(34, 'Roll Up 480 SL', '1 L', 0, 'L', NULL, NULL),
(35, 'Roll Up 480 SL', '5 L', 0, 'L', NULL, NULL),
(36, 'Roll Up 480 SL', '20 L', 0, 'L', NULL, NULL),
(37, 'Rexone 276 SL', '250 ML', 0, 'L', NULL, NULL),
(38, 'Rexone 276 SL', '500 ML', 0, 'L', NULL, NULL),
(39, 'Rexone 276 SL', '1 L', 0, 'L', NULL, NULL),
(40, 'Rexone 276 SL', '5 L', 0, 'L', NULL, NULL),
(41, 'Rexone 276 SL', '20 L', 0, 'L', NULL, NULL),
(42, 'Hantu', '250 ML', 0, 'L', NULL, NULL),
(43, 'Hantu', '500 ML', 0, 'L', NULL, NULL),
(44, 'Hantu', '1 L', 0, 'L', NULL, NULL),
(45, 'Generally 25 WP', '250 Gr', 0, 'KG', NULL, NULL),
(46, 'Zhipost 80 P', '15 Gr', 0, 'KG', NULL, NULL),
(47, 'Zhipost 80 P', '500 Gr', 0, 'KG', NULL, NULL),
(48, 'Sangit 50 EC', '100 ML', 0, 'L', NULL, NULL),
(49, 'Sangit 50 EC', '400 ML', 0, 'L', NULL, NULL),
(50, 'Astana 85 WP', '100 Gr', 0, 'KG', NULL, NULL),
(51, 'Hypotex 500 SL', '200 ML', 0, 'L', NULL, NULL),
(52, 'Hypotex 500 SL', '500 ML', 0, 'L', NULL, NULL),
(53, 'Hypotex 500 SL', '1 L', 0, 'L', NULL, NULL),
(54, 'Perekat Golden Stik', '1 L', 0, 'L', NULL, NULL),
(55, 'Kalsium 99', '1 KG', 0, 'KG', NULL, NULL),
(56, 'Kalsium Boron Manohara', '1 KG', 0, 'KG', NULL, NULL),
(57, 'Garuda', '1 KG', 0, 'KG', NULL, NULL),
(58, 'Diana', '1 KG', 0, 'KG', NULL, NULL),
(59, 'Urea Petro', '50 KG', 0, 'KG', NULL, NULL),
(60, 'NPK DGW 16 16 16', '1 KG', 0, 'KG', NULL, NULL),
(61, 'NPK DGW 16 16 16', '50 KG', 0, 'KG', NULL, NULL),
(62, 'NPK DGW 15 15 15', '1 KG', 0, 'KG', NULL, NULL),
(63, 'NPK DGW 15 15 15', '50 KG', 0, 'KG', NULL, NULL),
(64, 'MPK DGW', '1 KG', 0, 'KG', NULL, NULL),
(65, 'KNO3 DGW', '1 KG', 0, 'KG', NULL, NULL),
(66, 'KNO3 DGW', '50 KG', 0, 'KG', NULL, NULL),
(67, 'NPK Mutiara Grower', '1 KG', 0, 'KG', NULL, NULL),
(68, 'NPK Mutiara Grower', '25 KG', 0, 'KG', NULL, NULL),
(69, 'NPK Mutiara Grower', '50 KG', 0, 'KG', NULL, NULL),
(70, 'KCL Kalizer', '500 ML', 0, 'L', NULL, NULL),
(71, 'NPK Golden Max 21 21 21', '1 KG', 0, 'KG', NULL, NULL),
(72, 'NPK Golden Max 21 21 21', '20 KG', 0, 'KG', NULL, NULL),
(73, 'Mutiara Karate', '1 KG', 0, 'KG', NULL, NULL),
(74, 'TSP 46 Loying', '50 KG', 0, 'KG', NULL, NULL),
(75, 'Borate Dunia Subur 47%', '25 KG', 0, 'KG', NULL, NULL),
(76, 'ZA Loying', '50 KG', 0, 'KG', NULL, NULL),
(77, 'KCL Loying', '5 KG', 0, 'KG', NULL, NULL),
(78, 'KCL Loying', '50 KG', 0, 'KG', NULL, NULL),
(79, 'KCL Mahkota', '50 KG', 0, 'KG', NULL, NULL),
(80, 'NPK Among tani 16 16 16', '1 KG', 0, 'KG', NULL, NULL),
(81, 'SP 36 Daun Sawit', '50 KG', 0, 'KG', NULL, NULL),
(82, 'Vanda-Fur 3 GR', '1 KG', 0, 'KG', NULL, NULL),
(83, 'Vanda-Fur 3 GR', '2 KG', 0, 'KG', NULL, NULL),
(84, 'KCL Cair Kalizer', '500 ML', 0, 'L', NULL, NULL),
(85, 'KCL Cair Kalizer', '1 L', 0, 'L', NULL, NULL),
(86, 'Raja Phoska', '50 KG', 0, 'KG', NULL, NULL),
(87, 'Polibek', '35 x 40', 0, 'KG', NULL, NULL),
(88, 'Polibek', '25 x 30', 0, 'KG', NULL, NULL),
(89, 'Nagin', 'SP', 0, 'PCS', NULL, NULL),
(90, 'Nagin', '20 Gr', 0, 'PCS', NULL, NULL),
(91, 'Timun Adia', 'SP', 0, 'PCS', NULL, NULL),
(92, 'Timun Adia', '20 Gr', 0, 'PCS', NULL, NULL),
(93, 'Kacang Panjang The rorys F1', 'SP', 0, 'PCS', NULL, NULL),
(94, 'Kacang Panjang The rorys F1', '500 Gr', 0, 'PCS', NULL, NULL),
(95, 'Semangka Elvano Non Biji F1', '300 Butir', 0, 'PCS', NULL, NULL),
(96, 'Semangka Muthia F1 Non Biji', '300 Butir', 0, 'PCS', NULL, NULL),
(97, 'Jagung Manis Malengka', 'SP', 0, 'PCS', NULL, NULL),
(98, 'Jagung Manis Malengka', '250 Gr', 0, 'PCS', NULL, NULL),
(99, 'Melon Orange Viola F1', '550 Butir', 0, 'PCS', NULL, NULL),
(100, 'Buncis Piala', '50 Gr', 0, 'PCS', NULL, NULL),
(101, 'Buncis Piala', '500 Gr', 0, 'PCS', NULL, NULL),
(102, 'Kacang Panjang Amozon', '50 Gr', 0, 'PCS', NULL, NULL),
(103, 'Prematop', 'SP', 0, 'PCS', NULL, NULL),
(104, 'Stik Solo Hitam', '-', 0, 'PCS', NULL, NULL),
(105, 'Stik Solo Stainless', '-', 0, 'PCS', NULL, NULL),
(106, 'Selang Elektrik', '-', 0, 'PCS', NULL, NULL),
(107, 'Jamara F1', 'SP', 0, 'PCS', NULL, NULL),
(108, 'Jamara F1', '250 Gr', 0, 'PCS', NULL, NULL),
(109, 'Elip', 'SP', 0, 'PCS', NULL, NULL),
(110, 'Elip', '5 Gr', 0, 'PCS', NULL, NULL),
(111, 'Lexa', 'SP', 0, 'PCS', NULL, NULL),
(112, 'Lexa', '5 Gr', 0, 'PCS', NULL, NULL),
(113, 'Alif', 'SP', 0, 'PCS', NULL, NULL),
(114, 'Alif', '10 Gr', 0, 'PCS', NULL, NULL),
(115, 'Mila', 'SP', 0, 'PCS', NULL, NULL),
(116, 'Mila', '10 Gr', 0, 'PCS', NULL, NULL),
(117, 'Jalak', 'SP', 0, 'PCS', NULL, NULL),
(118, 'Jalak', '10 Gr', 0, 'PCS', NULL, NULL),
(119, 'Gendis', 'SP', 0, 'PCS', NULL, NULL),
(120, 'Gendis', '10 Gr', 0, 'PCS', NULL, NULL),
(121, 'Asoka', 'SP', 0, 'PCS', NULL, NULL),
(122, 'Asoka', '10 Gr', 0, 'PCS', NULL, NULL),
(123, 'Winis', 'SP', 0, 'PCS', NULL, NULL),
(124, 'Winis', '25 Gr', 0, 'PCS', NULL, NULL),
(125, 'Untavi', 'SP', 0, 'PCS', NULL, NULL),
(126, 'Untavi', '5 Gr', 0, 'PCS', NULL, NULL),
(127, 'Sigo', 'SP', 0, 'PCS', NULL, NULL),
(128, 'Sigo', '10 Gr', 0, 'PCS', NULL, NULL),
(129, 'Mangi', '5 Gr', 0, 'PCS', NULL, NULL),
(130, 'Mangi', '10 Gr', 0, 'PCS', NULL, NULL),
(131, 'Naruto', 'SP', 0, 'PCS', NULL, NULL),
(132, 'Naruto', '10 Gr', 0, 'PCS', NULL, NULL),
(133, 'Pojur', 'SP', 0, 'PCS', NULL, NULL),
(134, 'Pojur', '10 Gr', 0, 'PCS', NULL, NULL),
(135, 'Melinda', 'SP', 0, 'PCS', NULL, NULL),
(136, 'Melinda', '20 Gr', 0, 'PCS', NULL, NULL),
(137, 'Muri', 'SP', 0, 'PCS', NULL, NULL),
(138, 'Muri', '10 Gr', 0, 'PCS', NULL, NULL),
(139, 'Gerigi', 'SP', 0, 'PCS', NULL, NULL),
(140, 'Gerigi', '10 Gr', 0, 'PCS', NULL, NULL),
(141, 'Kaswari', 'SP', 0, 'PCS', NULL, NULL),
(142, 'Kaswari', '10 Gr', 0, 'PCS', NULL, NULL),
(143, 'Gotik', 'SP', 0, 'PCS', NULL, NULL),
(144, 'Gotik', '10 Gr', 0, 'PCS', NULL, NULL),
(145, 'Buntavi', 'SP', 0, 'PCS', NULL, NULL),
(146, 'Buntavi', '10 Gr', 0, 'PCS', NULL, NULL),
(147, 'Ponari', 'SP', 0, 'PCS', NULL, NULL),
(148, 'Ponari', '10 Gr', 0, 'PCS', NULL, NULL),
(149, 'Bayama', '50 Gr', 0, 'PCS', NULL, NULL),
(150, 'Bayama', '500 Gr', 0, 'PCS', NULL, NULL),
(151, 'Almino', 'SP', 0, 'PCS', NULL, NULL),
(152, 'Almino', '10 Gr', 0, 'PCS', NULL, NULL),
(153, 'Sicoy', 'SP', 0, 'PCS', NULL, NULL),
(154, 'Sicoy', '10 Gr', 0, 'PCS', NULL, NULL),
(155, 'Palapa', 'SP', 0, 'PCS', NULL, NULL),
(156, 'Palapa', '10 Gr', 0, 'PCS', NULL, NULL),
(157, 'Patria', 'SP', 0, 'PCS', NULL, NULL),
(158, 'Patria', '10 Gr', 0, 'PCS', NULL, NULL),
(159, 'Dakri', 'SP', 0, 'PCS', NULL, NULL),
(160, 'Dakri', '10 Gr', 0, 'PCS', NULL, NULL),
(161, 'Bokber', 'SP', 0, 'PCS', NULL, NULL),
(162, 'Bokber', '10 Gr', 0, 'PCS', NULL, NULL),
(163, 'ELZA', 'SP', 0, 'PCS', NULL, NULL),
(164, 'ELZA', '1,1 Gr', 0, 'PCS', NULL, NULL),
(165, 'ELZA', '5 Gr', 0, 'PCS', NULL, NULL),
(166, 'Sakoti', 'SP', 0, 'PCS', NULL, NULL),
(167, 'KBS 17 F', 'SP', 0, 'PCS', NULL, NULL),
(168, 'Satria', 'SP', 0, 'PCS', NULL, NULL),
(169, 'Lindo F1', 'SP', 0, 'PCS', NULL, NULL),
(170, 'Lindo F1', '5 Gr', 0, 'PCS', NULL, NULL),
(171, 'Linju', 'SP', 0, 'PCS', NULL, NULL),
(172, 'Linju', '5 Gr', 0, 'PCS', NULL, NULL),
(173, 'ZET', 'SP', 0, 'PCS', NULL, NULL),
(174, 'ZET', '10 Gr', 0, 'PCS', NULL, NULL),
(175, 'Cabe Pelangi', 'SP', 0, 'PCS', NULL, NULL),
(176, 'Isadora', 'SP', 0, 'PCS', NULL, NULL),
(177, 'Caesar', 'SP', 0, 'PCS', NULL, NULL),
(178, 'Tango', '50 Gr', 0, 'PCS', NULL, NULL),
(179, 'Kecipir', 'SP', 0, 'PCS', NULL, NULL),
(180, 'Lem Fox', '75 Gr', 0, 'PCS', NULL, NULL),
(181, 'Sprayer Boster', '2 L', 0, 'L', NULL, NULL),
(182, 'Tawas', '25 KG', 0, 'KG', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_02_03_061809_create_to_do_lists_table', 1),
(6, '2023_02_04_022148_create_manajemen_stoks_table', 1),
(7, '2023_02_08_093643_create_manajemen_members_table', 1),
(8, '2023_02_13_155652_create_invoices_table', 1),
(9, '2023_02_13_161058_create_detail_invoices_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `to_do_lists`
--

CREATE TABLE `to_do_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Wahyudianto', 'wahyudianto@gmail.com', NULL, '$2y$10$YrsnMnY7yeWHfkkwfr.uueKpGRV3F8WIWQa/OLc.e4VjPpCimipe.', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_invoices`
--
ALTER TABLE `detail_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_invoices_idpesanan_foreign` (`idPesanan`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_idmember_foreign` (`idMember`);

--
-- Indeks untuk tabel `manajemen_members`
--
ALTER TABLE `manajemen_members`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `manajemen_stoks`
--
ALTER TABLE `manajemen_stoks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `to_do_lists`
--
ALTER TABLE `to_do_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_invoices`
--
ALTER TABLE `detail_invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `manajemen_members`
--
ALTER TABLE `manajemen_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `manajemen_stoks`
--
ALTER TABLE `manajemen_stoks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `to_do_lists`
--
ALTER TABLE `to_do_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_invoices`
--
ALTER TABLE `detail_invoices`
  ADD CONSTRAINT `detail_invoices_idpesanan_foreign` FOREIGN KEY (`idPesanan`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_idmember_foreign` FOREIGN KEY (`idMember`) REFERENCES `manajemen_members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
