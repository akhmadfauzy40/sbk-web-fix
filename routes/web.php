<?php

use App\Http\Controllers\detailInvoiceController;
use App\Http\Controllers\halamanController;
use App\Http\Controllers\invoiceController;
use App\Http\Controllers\manajemenMemberController;
use App\Http\Controllers\manajemenStokController;
use App\Http\Controllers\sessionController;
use App\Http\Controllers\toDoListController;
use App\Http\Controllers\history;
use App\Models\detailInvoice;
use App\Models\manajemen_member;
use App\Models\manajemenStok;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [sessionController::class, 'index']);
Route::post('/login', [sessionController::class, 'login']);

Route::middleware(['auth'])->group(function () {
    Route::resource('/manajemen-produk', manajemenStokController::class);
    Route::resource('/manajemen-customer', manajemenMemberController::class);
    Route::resource('/welcome', toDoListController::class);
    Route::resource('/invoice-details', detailInvoiceController::class);
    Route::get('/invoice-details/create/{id}', [detailInvoiceController::class, 'create'])->name('invoice-details.create');
    Route::get('/invoice', [halamanController::class, 'invoice']);
    Route::get('/invoice/print/printInvoice/{id}', [InvoiceController::class, 'printInvoice'])->name('invoice.print.printInvoice');
    Route::get('/invoice/print/printSuratJalan/{id}', [InvoiceController::class, 'printSuratJalan'])->name('invoice.print.printSuratJalan');
    Route::resource('/invoiceMember', invoiceController::class);
    Route::get('/invoiceMember/create/{id}', [invoiceController::class, 'create'])->name('invoiceMember.create');
    Route::get('/rekap', [halamanController::class, 'rekap']);
    Route::get('/rekapBulanan', [halamanController::class, 'rekapBulanan']);
    Route::get('/rekapTahunan', [halamanController::class, 'rekapTahunan']);
    Route::get('/rekapMember', [halamanController::class, 'rekapMember']);
    Route::get('/rekap/print/printRekapBulanan/{bulanTahun}', [halamanController::class, 'printRekapBulanan'])->name('rekap.print.printRekapBulanan');
    Route::get('/rekap/print/printRekapTahunan/{tahun}', [halamanController::class, 'printRekapTahunan'])->name('rekap.print.printRekapTahunan');
    Route::get('/rekap/print/printRekapMember/{id}', [halamanController::class, 'printRekapMember'])->name('rekap.print.printRekapMember');
    Route::get('/about', [halamanController::class, 'about']);
    Route::get('/logout', [sessionController::class, 'logout']);
    Route::get('/manajemen-stok', function () {
        return view('manajemen_stok.index');
    });
    // Route::get('/input-stok', function(){
    //     return view('manajemen_stok.input');
    // });
    Route::get('input-stok', [manajemenStokController::class, 'toFormInput']);
    Route::get('history', [manajemenStokController::class, 'toHistory']);
    Route::resource('/stok', history::class);
});
