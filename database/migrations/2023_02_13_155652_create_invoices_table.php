<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->string('id')->primary()->unique();
            $table->unsignedBigInteger('idMember');
            $table->foreign('idMember')->references('id')->on('manajemen_members');
            $table->date('tanggalPesanan')->default(DB::raw('CURRENT_DATE'))->nullable(false);
            $table->enum('statusPesanan', ['Proses', 'Selesai'])->default('Proses');
            $table->integer('totalPembayaran')->default(0);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
