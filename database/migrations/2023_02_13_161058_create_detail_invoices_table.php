<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_invoices', function (Blueprint $table) {
            $table->id();
            $table->string('idPesanan');
            $table->string('idProduk');
            $table->string('namaProduk');
            $table->string('kemasan');
            $table->integer('harga');
            $table->integer('jumlah');
            $table->string('satuan');
            $table->integer('total');
            $table->timestamps();
            $table->foreign('idPesanan')->references('id')->on('invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_invoices');
    }
};
