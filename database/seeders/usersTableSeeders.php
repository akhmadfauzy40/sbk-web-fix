<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class usersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>1,
            'name'=>'Wahyudianto',
            'email'=>'wahyudianto@gmail.com',
            'password'=>Hash::make('beruntung123')
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Reaktif 490 SL',
            'kemasan'=>'250 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Reaktif 490 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Reaktif 490 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Reaktif 490 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Reaktif 490 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ralang 480 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ralang 480 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ralang 480 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ghoxone 138 SL',
            'kemasan'=>'250 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ghoxone 138 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ghoxone 138 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ghoxone 138 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ghoxone 138 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bround Up 240 SL',
            'kemasan'=>'250 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bround Up 240 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bround Up 240 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bround Up 240 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bround Up 240 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kontaxone 310 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kontaxone 310 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kontaxone 310 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kontaxone 310 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rolixone 276 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rolixone 276 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rolixone 276 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Primaxone 276 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Primaxone 276 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Primaxone 276 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Primaxone 276 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mantapxone 135 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mantapxone 135 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mantapxone 135 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Roll Up 480 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Roll Up 480 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Roll Up 480 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Roll Up 480 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rexone 276 SL',
            'kemasan'=>'250 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rexone 276 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rexone 276 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rexone 276 SL',
            'kemasan'=>'5 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Rexone 276 SL',
            'kemasan'=>'20 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hantu',
            'kemasan'=>'250 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hantu',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hantu',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Generally 25 WP',
            'kemasan'=>'250 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Zhipost 80 P',
            'kemasan'=>'15 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Zhipost 80 P',
            'kemasan'=>'500 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sangit 50 EC',
            'kemasan'=>'100 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sangit 50 EC',
            'kemasan'=>'400 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Astana 85 WP',
            'kemasan'=>'100 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hypotex 500 SL',
            'kemasan'=>'200 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hypotex 500 SL',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Hypotex 500 SL',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Perekat Golden Stik',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kalsium 99',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kalsium Boron Manohara',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Garuda',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Diana',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Urea Petro',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK DGW 16 16 16',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK DGW 16 16 16',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK DGW 15 15 15',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK DGW 15 15 15',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'MPK DGW',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KNO3 DGW',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KNO3 DGW',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Mutiara Grower',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Mutiara Grower',
            'kemasan'=>'25 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Mutiara Grower',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Kalizer',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Golden Max 21 21 21',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Golden Max 21 21 21',
            'kemasan'=>'20 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mutiara Karate',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'TSP 46 Loying',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Borate Dunia Subur 47%',
            'kemasan'=>'25 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ZA Loying',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Loying',
            'kemasan'=>'5 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Loying',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Mahkota',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'NPK Among tani 16 16 16',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'SP 36 Daun Sawit',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Vanda-Fur 3 GR',
            'kemasan'=>'1 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Vanda-Fur 3 GR',
            'kemasan'=>'2 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Cair Kalizer',
            'kemasan'=>'500 ML',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KCL Cair Kalizer',
            'kemasan'=>'1 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Raja Phoska',
            'kemasan'=>'50 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Polibek',
            'kemasan'=>'35 x 40',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Polibek',
            'kemasan'=>'25 x 30',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Nagin',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Nagin',
            'kemasan'=>'20 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Timun Adia',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Timun Adia',
            'kemasan'=>'20 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kacang Panjang The rorys F1',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kacang Panjang The rorys F1',
            'kemasan'=>'500 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Semangka Elvano Non Biji F1',
            'kemasan'=>'300 Butir',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Semangka Muthia F1 Non Biji',
            'kemasan'=>'300 Butir',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jagung Manis Malengka',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jagung Manis Malengka',
            'kemasan'=>'250 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Melon Orange Viola F1',
            'kemasan'=>'550 Butir',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Buncis Piala',
            'kemasan'=>'50 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Buncis Piala',
            'kemasan'=>'500 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kacang Panjang Amozon',
            'kemasan'=>'50 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Prematop',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Stik Solo Hitam',
            'kemasan'=>'-',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Stik Solo Stainless',
            'kemasan'=>'-',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Selang Elektrik',
            'kemasan'=>'-',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jamara F1',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jamara F1',
            'kemasan'=>'250 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Elip',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Elip',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Lexa',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Lexa',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Alif',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Alif',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mila',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mila',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jalak',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Jalak',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gendis',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gendis',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Asoka',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Asoka',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Winis',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Winis',
            'kemasan'=>'25 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Untavi',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Untavi',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sigo',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sigo',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mangi',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Mangi',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Naruto',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Naruto',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Pojur',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Pojur',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Melinda',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Melinda',
            'kemasan'=>'20 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Muri',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Muri',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gerigi',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gerigi',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kaswari',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kaswari',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gotik',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Gotik',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Buntavi',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Buntavi',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ponari',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Ponari',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bayama',
            'kemasan'=>'50 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bayama',
            'kemasan'=>'500 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Almino',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Almino',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sicoy',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sicoy',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Palapa',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Palapa',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Patria',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Patria',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Dakri',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Dakri',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bokber',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Bokber',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ELZA',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ELZA',
            'kemasan'=>'1,1 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ELZA',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sakoti',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'KBS 17 F',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Satria',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Lindo F1',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Lindo F1',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Linju',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Linju',
            'kemasan'=>'5 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ZET',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'ZET',
            'kemasan'=>'10 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Cabe Pelangi',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Isadora',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Caesar',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Tango',
            'kemasan'=>'50 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Kecipir',
            'kemasan'=>'SP',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Lem Fox',
            'kemasan'=>'75 Gr',
            'jumlahStok'=>'0',
            'satuan'=>'PCS',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Sprayer Boster',
            'kemasan'=>'2 L',
            'jumlahStok'=>'0',
            'satuan'=>'L',
        ]);

        DB::table('manajemen_stoks')->insert([
            'namaProduk'=>'Tawas',
            'kemasan'=>'25 KG',
            'jumlahStok'=>'0',
            'satuan'=>'KG',
        ]);



        // invoice 1 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'1',
        //     'namaMember'=>'Bpk. Mansyur',
        //     'namaToko'=>'',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id'=>'SBK/BJB-2301001',
        //     'idMember'=>'1',
        //     'tanggalPesanan'=>date('Y-m-d', strtotime('2023-01-02')),
        //     'statusPesanan'=>'Selesai',
        //     'totalPembayaran'=>10800000
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>1,
        //     'namaProduk'=>'Rexone 276 SL',
        //     'kemasan'=>'5 L',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'L',
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301001',
        //     'idProduk'=>'1',
        //     'namaProduk'=>'Rexone 276 SL',
        //     'kemasan'=>'5 L',
        //     'harga'=>54000,
        //     'jumlah'=>200,
        //     'satuan'=>'L',
        //     'total'=>10800000
        // ]);

        // invoice 1 bulan januari

        // invoice 2 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'2',
        //     'namaMember'=>'',
        //     'namaToko'=>'TK. Tani Makmur',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id' => 'SBK/BJB-2301002',
        //     'idMember' => '2',
        //     'tanggalPesanan' => date('Y-m-d', strtotime('2023-01-02')),
        //     'statusPesanan' => 'Selesai',
        //     'totalPembayaran' => 8230000
        // ]);        

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>2,
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'250 ML',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'L',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>3,
        //     'namaProduk'=>'Zhipost 80 P',
        //     'kemasan'=>'15 Gr',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'KG',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>4,
        //     'namaProduk'=>'Benih SP',
        //     'kemasan'=>'SP',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'PCS',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>5,
        //     'namaProduk'=>'Malengka',
        //     'kemasan'=>'250 Gr',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'PCS',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>6,
        //     'namaProduk'=>'Urea Petro',
        //     'kemasan'=>'50 KG',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'KG',
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301002',
        //     'idProduk'=>'2',
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'250 ML',
        //     'harga'=>60000,
        //     'jumlah'=>20,
        //     'satuan'=>'L',
        //     'total'=>1200000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301002',
        //     'idProduk'=>'3',
        //     'namaProduk'=>'Zhipost 80 P',
        //     'kemasan'=>'15 Gr',
        //     'harga'=>176666,
        //     'jumlah'=>3,
        //     'satuan'=>'KG',
        //     'total'=>530000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301002',
        //     'idProduk'=>'4',
        //     'namaProduk'=>'Benih SP',
        //     'kemasan'=>'SP',
        //     'harga'=>8000,
        //     'jumlah'=>75,
        //     'satuan'=>'PCS',
        //     'total'=>600000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301002',
        //     'idProduk'=>'5',
        //     'namaProduk'=>'Malengka',
        //     'kemasan'=>'250 Gr',
        //     'harga'=>37500,
        //     'jumlah'=>20,
        //     'satuan'=>'PCS',
        //     'total'=>750000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301002',
        //     'idProduk'=>'6',
        //     'namaProduk'=>'Urea Petro',
        //     'kemasan'=>'50 KG',
        //     'harga'=>515000,
        //     'jumlah'=>500,
        //     'satuan'=>'KG',
        //     'total'=>5150000
        // ]);

        // invoice 2 bulan januari

        // invoice 3 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'3',
        //     'namaMember'=>'Bpk. Wisnu',
        //     'namaToko'=>'',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id'=>'SBK/BJB-2301003',
        //     'idMember'=>'3',
        //     'tanggalPesanan'=>date('Y-m-d', strtotime('2023-01-03')),
        //     'statusPesanan'=>'Selesai',
        //     'totalPembayaran'=>13500000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301003',
        //     'idProduk'=>'6',
        //     'namaProduk'=>'Urea Petro',
        //     'kemasan'=>'50 KG',
        //     'harga'=>450000,
        //     'jumlah'=>1500,
        //     'satuan'=>'KG',
        //     'total'=>13500000
        // ]);

        // invoice 3 bulan januari

        // invoice 4 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'4',
        //     'namaMember'=>'',
        //     'namaToko'=>'TK Sahabat Tani',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id'=>'SBK/BJB-2301004',
        //     'idMember'=>'4',
        //     'tanggalPesanan'=>date('Y-m-d', strtotime('2023-01-03')),
        //     'statusPesanan'=>'Selesai',
        //     'totalPembayaran'=>750000
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>7,
        //     'namaProduk'=>'Vanda-Fur 3 GR',
        //     'kemasan'=>'1 KG',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'KG',
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301004',
        //     'idProduk'=>'7',
        //     'namaProduk'=>'Vanda-Fur 3 GR',
        //     'kemasan'=>'1 KG',
        //     'harga'=>15000,
        //     'jumlah'=>50,
        //     'satuan'=>'KG',
        //     'total'=>750000
        // ]);

        // invoice 4 bulan januari

        // invoice 5 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'5',
        //     'namaMember'=>'Bpk. Farouk',
        //     'namaToko'=>'',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id'=>'SBK/BJB-2301005',
        //     'idMember'=>'5',
        //     'tanggalPesanan'=>date('Y-m-d', strtotime('2023-01-03')),
        //     'statusPesanan'=>'Selesai',
        //     'totalPembayaran'=>2375000
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>8,
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'50 KG',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'KG',
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301005',
        //     'idProduk'=>'8',
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'50 KG',
        //     'harga'=>475000,
        //     'jumlah'=>250,
        //     'satuan'=>'KG',
        //     'total'=>2375000
        // ]);

        // invoice 5 bulan januari

        // invoice 6 bulan januari

        // DB::table('manajemen_members')->insert([
        //     'id'=>'6',
        //     'namaMember'=>'Bpk. Khairani',
        //     'namaToko'=>'',
        //     'alamatToko'=>'Banjarbaru',
        //     'nomorHP'=>''
        // ]);

        // DB::table('invoices')->insert([
        //     'id'=>'SBK/BJB-2301006',
        //     'idMember'=>'6',
        //     'tanggalPesanan'=>date('Y-m-d', strtotime('2023-01-04')),
        //     'statusPesanan'=>'Selesai',
        //     'totalPembayaran'=>9697500
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>9,
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'5 L',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'L',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>10,
        //     'namaProduk'=>'Elip',
        //     'kemasan'=>'SP',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'PCS',
        // ]);

        // DB::table('manajemen_stoks')->insert([
        //     'id'=>11,
        //     'namaProduk'=>'Benih SP',
        //     'kemasan'=>'SP',
        //     'jumlahStok'=>'0',
        //     'satuan'=>'PCS',
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301006',
        //     'idProduk'=>'9',
        //     'namaProduk'=>'Ghoxone 135 SL',
        //     'kemasan'=>'5 L',
        //     'harga'=>37000,
        //     'jumlah'=>200,
        //     'satuan'=>'L',
        //     'total'=>7400000
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301006',
        //     'idProduk'=>'10',
        //     'namaProduk'=>'Elip',
        //     'kemasan'=>'SP',
        //     'harga'=>8750,
        //     'jumlah'=>50,
        //     'satuan'=>'PCS',
        //     'total'=>437500
        // ]);

        // DB::table('detail_invoices')->insert([
        //     'idPesanan'=>'SBK/BJB-2301006',
        //     'idProduk'=>'11',
        //     'namaProduk'=>'Benih SP',
        //     'kemasan'=>'SP',
        //     'harga'=>7750,
        //     'jumlah'=>240,
        //     'satuan'=>'PCS',
        //     'total'=>1860000
        // ]);
        // invoice 6 bulan januari

    }
}
