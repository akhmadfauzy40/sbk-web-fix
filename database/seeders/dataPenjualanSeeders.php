<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class dataPenjualanSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Invoice 1 bulan maret 2023
        DB::table('invoices')->insert([
            'id'=>'SBK/BJB-2303001',
            'idMember'=>'1',
            'tanggalPesanan'=>date('Y-m-d', strtotime('2023-03-01')),
            'statusPesanan'=>'Selesai',
            'totalPembayaran'=>580000
        ]);

        DB::table('detail_invoices')->insert([
            'idPesanan'=>'SBK/BJB-2303001',
            'idProduk'=>'125',
            'namaproduk'=>'Untavi',
            'kemasan'=>'SP',
            'harga'=>8000,
            'jumlah'=>50,
            'satuan'=>'PCS',
            'total'=>400000
        ]);

        DB::table('detail_invoices')->insert([
            'idPesanan'=>'SBK/BJB-2303001',
            'idProduk'=>'135',
            'namaproduk'=>'Melinda',
            'kemasan'=>'SP',
            'harga'=>9000,
            'jumlah'=>20,
            'satuan'=>'PCS',
            'total'=>180000
        ]);

        // invoice 2 bulan maret 2023

        DB::table('invoices')->insert([
            'id'=>'SBK/BJB-2303002',
            'idMember'=>'2',
            'tanggalPesanan'=>date('Y-m-d', strtotime('2023-03-01')),
            'statusPesanan'=>'Selesai',
            'totalPembayaran'=>2025000
        ]);

        DB::table('detail_invoices')->insert([
            'idPesanan'=>'SBK/BJB-2303002',
            'idProduk'=>'75',
            'namaproduk'=>'Borate Dunia Subur 47%',
            'kemasan'=>'25 KG',
            'harga'=>27000,
            'jumlah'=>75,
            'satuan'=>'KG',
            'total'=>2025000
        ]);

        // invoice 3 bulan maret 2023

        DB::table('invoices')->insert([
            'id'=>'SBK/BJB-2303003',
            'idMember'=>'3',
            'tanggalPesanan'=>date('Y-m-d', strtotime('2023-03-02')),
            'statusPesanan'=>'Selesai',
            'totalPembayaran'=>18000000
        ]);

        DB::table('detail_invoices')->insert([
            'idPesanan'=>'SBK/BJB-2303003',
            'idProduk'=>'55',
            'namaproduk'=>'Kalsium 99',
            'kemasan'=>'1 KG',
            'harga'=>18000,
            'jumlah'=>1000,
            'satuan'=>'KG',
            'total'=>18000000
        ]);

    }
}
