<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class taskTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('to_do_lists')->insert(['task'=>'Jalan sama novi']);
        DB::table('to_do_lists')->insert(['task'=>'makan sama novi']);
        DB::table('to_do_lists')->insert(['task'=>'foto bareng novi']);
        DB::table('to_do_lists')->insert(['task'=>'bahagia bareng novi']);
    }
}