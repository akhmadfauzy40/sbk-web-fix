<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class manajemenStok extends Model
{
    use HasFactory;
    protected $table = 'manajemen_stoks';
    protected $guarded = ['id'];
}
