<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    use HasFactory;
    protected $table = 'invoices';

    public $incrementing = false;

    public function manajemenMember()
    {
        return $this->belongsTo(manajemen_member::class, 'idMember');
    }
}
