<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class historyProduk extends Model
{
    use HasFactory;
    protected $table = 'history_produks';
    protected $guarded = ['id'];
}
