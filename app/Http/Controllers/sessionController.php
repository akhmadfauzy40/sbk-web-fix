<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class sessionController extends Controller
{
    function index()
    {
        return view("login.index");
    }

    function login(Request $request)
    {

        Session::flash('email', $request->email);

        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'Email Wajib Diisi',
            'password.required' => 'Password Wajib Diisi'
        ]);

        $infoLogin = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($infoLogin)) {
            $user = Auth::user();
            Session::put('name', $user->name);
            return redirect('/welcome')->with('success', 'Berhasil Login');
        } else {
            return redirect('/')->withErrors('Username dan Password yang di masukkan tidak valid');
        }
    }

    function logout(){
        Auth::logout();
        return redirect('/');
    }
}
