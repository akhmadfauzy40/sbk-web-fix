<?php

namespace App\Http\Controllers;

use App\Models\detailInvoice;
use App\Models\invoice;
use App\Models\manajemen_member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class invoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tahun_bulan = date('ym');
        $invoice = new invoice;
        $i = 1;

        // Loop untuk mencari id invoice yang belum ada di database
        while (invoice::find('SBK/BJB-' . $tahun_bulan . sprintf('%03d', $i))) {
            $i++;
        }

        $invoice->id = 'SBK/BJB-' . $tahun_bulan . sprintf('%03d', $i);
        $invoice->idMember = $id;

        if ($invoice->save()) {
            return redirect(route('invoiceMember.show', $id));
        } else {
            return redirect(route('invoiceMember.show', $id));
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('invoice.invoiceMember')->with([
            'invoices' => DB::table('invoices')->where('idMember', $id)->orderByDesc('id')->get(),
            'member' => manajemen_member::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoiceId = base64_decode($id);
        $invoice = invoice::find($invoiceId);

        $invoice->statusPesanan = 'Selesai';
        $invoice->totalPembayaran = $request->totalBayar;

        if ($invoice->save()) {
            return redirect(route('invoice-details.create', base64_encode($invoiceId)))->with('success', 'berhasil');
        } else {
            return redirect(route('invoice-details.create', base64_encode($invoiceId)))->with('error', 'gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoiceId = base64_decode($id);
        $invoice = invoice::find($invoiceId);
        $idMember = $invoice->idMember;

        if ($invoice->delete()) {
            return redirect(route('invoiceMember.show', $idMember));
        }
    }

    public function printInvoice($id)
    {
        // ambil data invoice dari database
        $invoiceId = base64_decode($id);
        $invoices = invoice::findOrFail($invoiceId);
        $member = manajemen_member::join('invoices', 'invoices.idMember', '=', 'manajemen_members.id')
            ->where('invoices.id', $invoiceId)
            ->select('manajemen_members.*')
            ->first();
        $detailInvoices = detailInvoice::where('idPesanan', $invoiceId)->get();

        return view('invoice.print.printInvoice', compact('invoiceId', 'invoices', 'member', 'detailInvoices'));
    }

    public function printSuratJalan($id)
    {
        // ambil data invoice dari database
        $invoiceId = base64_decode($id);
        $invoices = invoice::findOrFail($invoiceId);
        $member = manajemen_member::join('invoices', 'invoices.idMember', '=', 'manajemen_members.id')
            ->where('invoices.id', $invoiceId)
            ->select('manajemen_members.*')
            ->first();
        $detailInvoices = detailInvoice::where('idPesanan', $invoiceId)->get();

        return view('invoice.print.printSuratJalan', compact('invoiceId', 'invoices', 'member', 'detailInvoices'));
    }
}
