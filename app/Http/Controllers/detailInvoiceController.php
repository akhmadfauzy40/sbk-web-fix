<?php

namespace App\Http\Controllers;

use App\Models\detailInvoice;
use App\Models\invoice;
use App\Models\manajemen_member;
use App\Models\manajemenStok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class detailInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $invoiceId = base64_decode($id);
        $invoices = invoice::find($invoiceId);
        $member = manajemen_member::join('invoices', 'invoices.idMember', '=', 'manajemen_members.id')
            ->where('invoices.id', $invoiceId)
            ->select('manajemen_members.*')
            ->first();
        $detailInvoices = detailInvoice::where('idPesanan', $invoiceId)->get();
        $products = manajemenStok::where('jumlahStok', '>', 0)->get();

        return view('invoice.detailPesanan', compact('invoices', 'member', 'detailInvoices', 'invoiceId', 'products'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = new detailInvoice;
        $produk->idPesanan = $request->id;
        $produk->idProduk = $request->idProduk;
        $produk->namaProduk = DB::table('manajemen_stoks')->where('id', $request->idProduk)->value('namaProduk');
        $produk->kemasan = DB::table('manajemen_stoks')->where('id', $request->idProduk)->value('kemasan');
        $produk->harga = $request->harga;
        $produk->satuan = DB::table('manajemen_stoks')->where('id', $request->idProduk)->value('satuan');
        $produk->jumlah = $request->jumlah;
        $total = $produk->harga * $request->jumlah;
        $produk->total = $total;

        $sisaStok = manajemenStok::find($request->idProduk);
        $sisaStok->jumlahStok -= $request->jumlah;

        if($produk->save() && $sisaStok->save()) {
            return redirect(route('invoice-details.create', base64_encode($request->id)))->with('success', 'berhasil');
        } else {
            return redirect(route('invoice-details.create', base64_encode($request->id)))->with('error', 'gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detailInvoice = detailInvoice::find($id);
        $idPesanan = $detailInvoice->idPesanan;
        $jumlahProduk = $detailInvoice->jumlah;

        $restoreStok = manajemenStok::find($detailInvoice->idProduk);
        $restoreStok->jumlahStok += $jumlahProduk;

        if($restoreStok->save() && $detailInvoice->delete()){
            return redirect(route('invoice-details.create', base64_encode($idPesanan)))->with('success', 'berhasil');
        }else{
            return redirect(route('invoice-details.create', base64_encode($idPesanan)))->with('error', 'gagal');
        }

    }
}
