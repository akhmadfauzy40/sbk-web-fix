<?php

namespace App\Http\Controllers;

use App\Models\historyProduk;
use App\Models\manajemenStok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class manajemenStokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('manajemen_produk.index')->with([
            'manajemenStok' => manajemenStok::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaProduk' => 'required',
            'kemasan' => 'required',
            'jumlahStok' => 'required',
            'satuan' => 'required',
        ]);

        $produk = new manajemenStok;
        $produk->namaProduk = $request->namaProduk;
        $produk->kemasan = $request->kemasan;
        $produk->jumlahStok = $request->jumlahStok;
        $produk->satuan = $request->satuan;

        $produk->save();
        return redirect('manajemen-produk')->with('success', 'berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namaProduk' => 'required',
            'kemasan' => 'required',
            'satuan' => 'required',
        ]);

        $produk = manajemenStok::find($id);
        $produk->namaProduk = $request->namaProduk;
        $produk->kemasan = $request->kemasan;
        $produk->satuan = $request->satuan;

        $produk->save();
        return redirect('manajemen-produk')->with('update', 'berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = manajemenStok::find($id);
        $produk->delete();

        return redirect('manajemen-produk')->with('hapus', 'berhasil');
    }

    public function toFormInput()
    {
        $history = DB::table('history_produks')
            ->join('manajemen_stoks', 'history_produks.idProduk', '=', 'manajemen_stoks.id')
            ->select(
                'history_produks.id',
                'history_produks.tanggal',
                'history_produks.idProduk',
                'history_produks.jumlahMasuk',
                'manajemen_stoks.namaProduk',
                'manajemen_stoks.kemasan',
                'manajemen_stoks.satuan'
            )
            ->orderBy('history_produks.created_at', 'DESC')
            ->limit(5)
            ->get();

        return view('manajemen_stok.input')->with([
            'produks' => manajemenStok::all(),
            'historys' => $history
        ]);
    }
    public function toHistory()
    {
        $history = DB::table('history_produks')
            ->join('manajemen_stoks', 'history_produks.idProduk', '=', 'manajemen_stoks.id')
            ->select(
                'history_produks.id',
                'history_produks.tanggal',
                'history_produks.idProduk',
                'history_produks.jumlahMasuk',
                'manajemen_stoks.namaProduk',
                'manajemen_stoks.kemasan',
                'manajemen_stoks.satuan'
            )
            ->orderBy('history_produks.created_at', 'DESC')
            ->get();

        return view('manajemen_stok.history')->with([
            'historys' => $history
        ]);
    }
}
