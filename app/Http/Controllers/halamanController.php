<?php

namespace App\Http\Controllers;

use App\Models\detailInvoice;
use Carbon\Carbon;
use App\Models\invoice;
use App\Models\manajemen_member;
use App\Models\manajemenStok;
use Illuminate\Http\Request;

class halamanController extends Controller
{
    // function welcome(){
    //     return view("welcome");
    // }

    function manajemenStok()
    {
        return view("manajemen_stok.index");
    }

    function invoice()
    {
        return view("invoice.index")->with([
            'listMember' => manajemen_member::all()
        ]);
    }

    function rekap()
    {
        return view("rekap.index");
    }

    function about()
    {
        return view("about.index");
    }

    function rekapBulanan()
    {
        setlocale(LC_TIME, 'id_ID.utf8');
        $listBulan = Invoice::distinct()
            ->selectRaw('DATE_FORMAT(tanggalPesanan, "%M %Y") AS bulan')
            ->orderBy('tanggalPesanan', 'desc')
            ->get()
            ->pluck('bulan');

        $listBulanTeks = [];
        $listBulanNormal = [];
        foreach ($listBulan as $bulan) {
            $listBulanTeks[] = Carbon::parse($bulan)->locale('id')->isoFormat('MMMM Y');
            $listBulanNormal[] = Carbon::parse($bulan)->format('Y-m');
        }

        return view('rekap.rekapBulanan', compact('listBulanTeks', 'listBulan', 'listBulanNormal'));
    }

    function rekapTahunan()
    {
        $listTahun = Invoice::distinct()
            ->selectRaw('YEAR(tanggalPesanan) as tahun')
            ->where('statusPesanan', 'Selesai')
            ->orderBy('tanggalPesanan', 'desc')
            ->get()
            ->pluck('tahun');

        return view('rekap.rekapTahunan', compact('listTahun'));
    }

    function rekapMember()
    {
        $listMembers = manajemen_member::all();

        return view('rekap.rekapMember', compact('listMembers'));
    }

    public function printRekapBulanan($bulanTahun)
    {
        $invoices = Invoice::whereYear('tanggalPesanan', Carbon::parse($bulanTahun)->year)
            ->whereMonth('tanggalPesanan', Carbon::parse($bulanTahun)->month)
            ->where('statusPesanan', 'Selesai')
            ->select('id', 'idMember', 'tanggalPesanan', 'totalPembayaran')
            ->with('manajemenMember')
            ->get();

        $detailInvoices = detailInvoice::all();
        $bulanTahun = Carbon::parse($bulanTahun)->locale('id')->isoFormat('MMMM Y');
        return view('rekap.print.printRekapBulanan', compact('invoices', 'bulanTahun', 'detailInvoices'));
    }

    public function printRekapTahunan($tahun)
    {
        $invoices = Invoice::whereYear('tanggalPesanan', $tahun)
            ->where('statusPesanan', 'Selesai')
            ->select('id', 'idMember', 'tanggalPesanan', 'totalPembayaran')
            ->with('manajemenMember')
            ->get();

        $detailInvoices = detailInvoice::all();
        $tahun = Carbon::parse($tahun)->locale('id')->year;
        return view('rekap.print.printRekapTahunan', compact('invoices', 'tahun', 'detailInvoices'));
    }

    public function printRekapMember($id)
    {
        $invoices = Invoice::where('idMember', $id)
            ->where('statusPesanan', 'Selesai')
            ->select('id', 'idMember', 'tanggalPesanan', 'totalPembayaran')
            ->get();

        $member = manajemen_member::find($id);
        $detailInvoices = DetailInvoice::all();
        return view('rekap.print.printRekapMember', compact('invoices', 'detailInvoices', 'member'));
    }
}
