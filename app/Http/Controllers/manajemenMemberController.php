<?php

namespace App\Http\Controllers;

use App\Models\manajemen_member;
use Illuminate\Http\Request;

class manajemenMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manajemen_customer.index')->with([
            'manajemenMember' => manajemen_member::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'alamatToko'=>'required'
        ]);

        $member = new manajemen_member;
        $member->namaMember = $request->namaMember;
        $member->namaToko = $request->namaToko;
        $member->alamatToko = $request->alamatToko;
        $member->nomorHP = $request->nomorHP;

        if($member->save()) {
            return redirect('manajemen-customer')->with('success', 'berhasil');
        } else {
            return redirect('manajemen-customer')->with('error', 'gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namaMember'=>'required',
            'namaToko'=>'required',
            'nomorHP'=>'required',
            'alamatToko'=>'required'
        ]);

        $member = manajemen_member::find($id);
        $member->namaMember = $request->namaMember;
        $member->namaToko = $request->namaToko;
        $member->alamatToko = $request->alamatToko;
        $member->nomorHP = $request->nomorHP;

        if($member->save()) {
            return redirect('manajemen-customer')->with('update', 'berhasil');
        } else {
            return redirect('manajemen-customer')->with('error', 'gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = manajemen_member::find($id);

        if($member->delete()){
            return redirect('manajemen-customer')->with('hapus', 'berhasil');
        }else{
            return redirect('manajemen-customer')->with('error', 'gagal');
        }
    }
}
