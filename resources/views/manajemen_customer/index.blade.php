@extends('layout/layout')

@section('title')
    Manajemen Member - SBK-Web
@endsection

@section('sidebar')
    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="/welcome" class="navbar-brand mx-4 mb-3">
                <h3 class="text-primary"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;">&nbsp CV. SBK</h3>
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="{{ asset('img/person.png') }}" alt=""
                        style="width: 40px; height: 40px;">
                    <div
                        class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                    </div>
                </div>
                <div class="ms-3">
                    <h6 class="mb-0">{{ session('name') }}</h6>
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="/welcome" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                <a href="/manajemen-produk" class="nav-item nav-link" style="font-size: 14px;"><i class="fa fa-th me-2"></i>Manajemen Produk</a>
                <a href="/manajemen-stok" class="nav-item nav-link"><i class="fa fa-cubes me-2"></i>Manajemen Stok</a>
                <a href="/manajemen-customer" class="nav-item nav-link active"><i class="fa fa-user me-2"></i>Membership</a>
                <a href="/invoice" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Pesanan</a>
                <a href="/rekap" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Rekap</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->
@endsection


@section('konten')
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="welcome" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><img src="{{ asset('img/logo2.png') }}" alt=""
                            style="height: 45px; width: 45px;"></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">

                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('img/person.png') }}" alt=""
                                style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">{{ session('name') }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                            <a href="/logout" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Recent Sales Start -->
            <div class="container-fluid pt-4 px-4">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Member Baru Berhasil Disimpan
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Maaf Terjadi Kesalahan
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (session('update'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Member Berhasil Diperbarui
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (session('hapus'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Member Berhasil Dihapus
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="bg-secondary text-center rounded p-4">
                    <h1 class="display-6 mb-0">Manajemen Member CV. SBK</h1>
                </div>
            </div>
            <!-- Recent Sales End -->


            <!-- Widgets Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="bg-secondary rounded h-100 p-4">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-0">List Membership</h6>
                                <button type="button" class="btn btn-outline-success btn-sm m-2" data-bs-toggle="modal"
                                    data-bs-target="#tambahData">Tambah Member</button>
                                {{-- <a href="">Show All</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tbl_produk">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Member</th>
                                            <th scope="col">Nama Toko</th>
                                            <th scope="col">Nomor HP</th>
                                            <th scope="col">Alamat Toko</th>
                                            <th scope="col" class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($manajemenMember as $no => $item)
                                            <tr>
                                                <th scope="row">{{ $no + 1 }}</th>
                                                <td>{{ $item->namaMember }}</td>
                                                <td>{{ $item->namaToko }}</td>
                                                <td>{{ $item->nomorHP }}</td>
                                                <td>{{ $item->alamatToko }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-outline-warning m-2 btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#edit{{ $no }}">Edit</button>
                                                    <button type="button" class="btn btn-outline-primary m-2 btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#hapus{{ $no }}">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            {{-- Start Modal --}}

                                            <div class="modal fade" id="hapus{{ $no }}" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form action="{{ route('manajemen-customer.destroy', $item->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-secondary">
                                                                <h5 class="modal-title text-primary"
                                                                    id="exampleModalLabel">Peringatan!!
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body bg-secondary">
                                                                Apakah Anda Yakin Ingin Menghapus Data Member Ini?
                                                            </div>
                                                            <div class="modal-footer bg-secondary">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" name="submit"
                                                                    class="btn btn-outline-primary m-2 btn-sm">Hapus</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="modal fade" id="edit{{ $no }}"
                                                data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                                                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <form action="{{ route('manajemen-customer.update', $item->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-secondary">
                                                                <h5 class="modal-title" id="staticBackdropLabel">Edit Data
                                                                    Member</h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body bg-secondary">
                                                                <div class="row mb-3">
                                                                    <label for="namaMember{{ $no }}"
                                                                        class="col-sm-2 col-form-label">Nama Member</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="namaMember"
                                                                            class="form-control" id="namaMember{{ $no }}"
                                                                            value="{{ $item->namaMember }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="namaToko{{ $no }}"
                                                                        class="col-sm-2 col-form-label">Nama Toko</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="namaToko"
                                                                            class="form-control" id="namaToko{{ $no }}"
                                                                            value="{{ $item->namaToko }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="nomorHP{{ $no }}"
                                                                        class="col-sm-2 col-form-label">Nomor HP</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="nomorHP"
                                                                            class="form-control" id="nomorHP{{ $no }}"
                                                                            value="{{ $item->nomorHP }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="alamatToko{{ $no }}"
                                                                        class="col-sm-2 col-form-label">Alamat Toko</label>
                                                                    <div class="col-sm-10">
                                                                        <textarea class="form-control" name="alamatToko" placeholder="Masukan Alamat Toko" id="alamatToko{{ $no }}"
                                                                            style="height: 150px;">{{ $item->alamatToko }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-secondary">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success"
                                                                    name="submit">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endforeach
                                        {{-- End Modal --}}
                                    </tbody>
                                </table>
                                {{-- Modal Tambah Data --}}
                                <form action="{{ route('manajemen-customer.store') }}" method="POST">
                                    @csrf
                                    <div class="modal fade" id="tambahData" data-bs-backdrop="static"
                                        data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header bg-secondary">
                                                    <h5 class="modal-title" id="staticBackdropLabel">Tambah Member</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body bg-secondary">
                                                    <div class="row mb-3">
                                                        <label for="namaMember-tambah" class="col-sm-2 col-form-label">Nama
                                                            Member</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="namaMember" class="form-control"
                                                                id="namaMember-tambah" placeholder="Masukan Nama Member"
                                                                required>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="namaToko-tambah" class="col-sm-2 col-form-label">Nama
                                                            Toko</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="namaToko" class="form-control"
                                                                id="namaToko-tambah" placeholder="Masukan Nama Toko" required>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="nomorHP-tambah" class="col-sm-2 col-form-label">Nomor HP</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="nomorHP" class="form-control"
                                                                id="nomorHP-tambah" placeholder="Contoh : +62812***">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="alamatToko-tambah" class="col-sm-2 col-form-label">Alamat
                                                            Toko</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" name="alamatToko" id="alamatToko-tambah" placeholder="Masukan Alamat Toko"
                                                                style="height: 150px;"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer bg-secondary">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" name="submit"
                                                        class="btn btn-success">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{-- Modal Tambah Data --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Widgets End -->


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>
@endsection
