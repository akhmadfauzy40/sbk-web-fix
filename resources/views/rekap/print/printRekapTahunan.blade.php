<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Rekap Bulan</title>
    <style>
        .header {
            height: 15cm;
            width: 25cm;
            margin: 0 auto;
        }

        .header .judul table tr {
            border: none;
        }

        .header .detailInvoice {
            padding-top: 75px;
        }

        .header .detailInvoice table td,
        .detailInvoice table tr {
            border: none;
        }

        .content .listBarang h2 {
            text-align: center;
        }

        .content .listBarang table {
            width: 850px;
            text-align: center;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="content">
            <div class="listBarang">
                <h2>REKAP PENJUALAN TAHUN {{ $tahun }}</h2>
                <table border="1">
                    <thead>
                        <td>No</td>
                        <td>Nomor Invoice</td>
                        <td>Tanggal Pemesanan</td>
                        <td>Nama Toko Member</td>
                        <td>Produk yang di Pesan</td>
                        <td>Total Pembayaran</td>
                    </thead>
                    <tbody>
                        @php
                            $totalMasuk = 0;
                        @endphp
                        @foreach ($invoices as $no => $invoice)
                            <tr>
                                <td>{{ $no + 1 }}</td>
                                <td>{{ $invoice->id }}</td>
                                <td>{{ \Carbon\Carbon::parse($invoice->tanggalPesanan)->format('d-M-Y') }}</td>
                                <td>{{ $invoice->manajemenMember->namaToko }}</td>
                                <td style="text-align: left;">
                                    <ul>
                                        @foreach ($detailInvoices->where('idPesanan', $invoice->id) as $item)
                                            <li>{{ $item->namaProduk.' '.$item->kemasan. ' (' . $item->jumlah . ' ' . $item->satuan . ')' }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ 'Rp.' . number_format($invoice->totalPembayaran, 0, ',', '.') }}</td>
                            </tr>
                            @php
                                $totalMasuk += $invoice->totalPembayaran;
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan="4">Jumlah Pemasukan</td>
                            <td></td>
                            <td>{{ 'Rp.' . number_format($totalMasuk, 0, ',', '.') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script>
    window.print();
</script>

</html>
