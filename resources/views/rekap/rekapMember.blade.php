@extends('layout/layout')

@section('title')
    Rekap Pembelian Member - SBK-Web
@endsection

@section('sidebar')
    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="/welcome" class="navbar-brand mx-4 mb-3">
                <h3 class="text-primary"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;">&nbsp CV. SBK</h3>
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="{{ asset('img/person.png') }}" alt=""
                        style="width: 40px; height: 40px;">
                    <div
                        class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                    </div>
                </div>
                <div class="ms-3">
                    <h6 class="mb-0">{{ session('name') }}</h6>
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="/welcome" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                <a href="/manajemen-produk" class="nav-item nav-link" style="font-size: 14px;"><i class="fa fa-th me-2"></i>Manajemen Produk</a>
                <a href="/manajemen-stok" class="nav-item nav-link"><i class="fa fa-cubes me-2"></i>Manajemen Stok</a>
                <a href="/manajemen-customer" class="nav-item nav-link"><i class="fa fa-user me-2"></i>Membership</a>
                <a href="/invoice" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Pesanan</a>
                <a href="/rekap" class="nav-item nav-link active"><i class="fa fa-chart-bar me-2"></i>Rekap</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->
@endsection


@section('konten')
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->

        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="welcome" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><img src="{{ asset('img/logo2.png') }}" alt=""
                            style="height: 45px; width: 45px;"></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">

                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('img/person.png') }}" alt=""
                                style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">{{ session('name') }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                            <a href="/logout" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Recent Sales Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary text-center rounded p-4">
                    <h1 class="display-6 mb-0">REKAP PEMBELIAN TIAP MEMBER</h1>
                </div>
            </div>
            <!-- Recent Sales End -->


            <!-- Widgets Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    @if ($listMembers->isEmpty())
                        <div class="col-sm-12 col-md-6 col-xl-4">
                            <div class="h-100 bg-secondary rounded p-4">
                                <h4 class="mb-4">Tidak Ada Data Penjualan</h4>
                            </div>
                        </div>
                    @else
                        @foreach ($listMembers as $member)
                            <div class="col-sm-12 col-md-6 col-xl-4">
                                <div class="h-100 bg-secondary rounded p-4">
                                    <h4 class="mb-4">Rekap Penjualan </br>{{ $member->namaToko }}</h4>
                                    <a href="{{ route('rekap.print.printRekapMember', $member->id) }}"
                                        class="text-decoration-none link-success d-flex align-items-center justify-content-end"
                                        target="blank">
                                        Cetak Dokumen Rekap
                                        <i class="bi bi-printer ms-2"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>

            <!-- Widgets End -->


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>
@endsection
