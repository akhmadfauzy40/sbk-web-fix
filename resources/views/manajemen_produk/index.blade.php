@extends('layout/layout')

@section('title')
    Manajemen Produk - SBK-Web
@endsection

@section('sidebar')
    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="/welcome" class="navbar-brand mx-4 mb-3">
                <h3 class="text-primary"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;">&nbsp CV. SBK</h3>
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="{{ asset('img/person.png') }}" alt=""
                        style="width: 40px; height: 40px;">
                    <div
                        class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                    </div>
                </div>
                <div class="ms-3">
                    <h6 class="mb-0">{{ session('name') }}</h6>
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="/welcome" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                <a href="/manajemen-produk" class="nav-item nav-link active" style="font-size: 14px;"><i class="fa fa-th me-2"></i>Manajemen Produk</a>
                <a href="/manajemen-stok" class="nav-item nav-link"><i class="fa fa-cubes me-2"></i>Manajemen Stok</a>
                <a href="/manajemen-customer" class="nav-item nav-link"><i class="fa fa-user me-2"></i>Membership</a>
                <a href="/invoice" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Pesanan</a>
                <a href="/rekap" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Rekap</a
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->
@endsection


@section('konten')
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->

        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="welcome" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;"></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">

                    </div>
                    <div class="nav-item dropdown">
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                                <img class="rounded-circle me-lg-2" src="{{ asset('img/person.png') }}" alt=""
                                    style="width: 40px; height: 40px;">
                                <span class="d-none d-lg-inline-flex">{{ session('name') }}</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                                <a href="/logout" class="dropdown-item">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Recent Sales Start -->
            <div class="container-fluid pt-4 px-4">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Produk Baru Berhasil Disimpan
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (session('update'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Produk Berhasil Diperbarui
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if (session('hapus'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <i class="fa fa-exclamation-circle me-2"></i>Data Produk Berhasil Dihapus
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="bg-secondary text-center rounded p-4">
                    <h1 class="display-6 mb-0">Manajemen Produk</h1>
                </div>
            </div>
            <!-- Recent Sales End -->


            <!-- Widgets Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="bg-secondary rounded h-100 p-4">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-0">List Produk</h6>
                                <button type="button" class="btn btn-outline-success m-2 btn-sm" data-bs-toggle="modal"
                                    data-bs-target="#staticBackdrop">
                                    Tambah Produk
                                </button>
                                {{-- <a href="">Show All</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tbl_produk">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Produk</th>
                                            <th scope="col">Kemaasan</th>
                                            <th scope="col">Jumlah Stok</th>
                                            <th scope="col" class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($manajemenStok as $no => $item)
                                            <tr>
                                                <th scope="row">{{ $no + 1 }}</th>
                                                <td>{{ $item->namaProduk }}</td>
                                                <td>{{ $item->kemasan }}</td>
                                                <td>{{ $item->jumlahStok." ".$item->satuan }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-outline-warning m-2 btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#edit{{ $no }}">Edit</button>
                                                    <button type="button" class="btn btn-outline-primary m-2 btn-sm"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#exampleModal{{ $no }}">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            {{-- Start Modal --}}

                                            <div class="modal fade" id="exampleModal{{ $no }}" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form action="{{ route('manajemen-produk.destroy', $item->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-secondary">
                                                                <h5 class="modal-title text-primary" id="exampleModalLabel">Peringatan!!
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body bg-secondary">
                                                                Apakah Anda Yakin Ingin Menghapus Data Produk Ini?
                                                            </div>
                                                            <div class="modal-footer bg-secondary">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" name="submit"
                                                                    class="btn btn-outline-primary m-2 btn-sm">Hapus</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="modal fade" id="edit{{ $no }}"
                                                data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                                                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                <form action="{{ route('manajemen-produk.update', $item->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-secondary">
                                                                <h5 class="modal-title" id="staticBackdropLabel">Edit Produk</h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body bg-secondary">
                                                                <div class="row mb-3">
                                                                    <label for="namaProduk{{ $no }}" class="col-sm-2 col-form-label">Nama Produk</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="namaProduk" class="form-control" id="namaProduk{{ $no }}"
                                                                            required value="{{ $item->namaProduk }}" placeholder="Masukkan Nama Produk">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="kemasan{{ $no }}" class="col-sm-2 col-form-label">Kemasan</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" value="{{ $item->kemasan }}" name="kemasan" class="form-control" id="kemasan{{ $no }}" placeholder="Contoh : 250 ml">
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label for="satuan{{ $no }}" class="col-sm-2 col-form-label">Satuan Produk</label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-select mb-3" aria-label="Default select example" id="satuan{{ $no }}" name="satuan">
                                                                            <option value="KG" {{ $item->satuan == "KG" ? "selected" : "" }}>Kilogram (KG)</option>
                                                                            <option value="L" {{ $item->satuan == "L" ? "selected" : "" }}>Liter (L)</option>
                                                                            <option value="PCS" {{ $item->satuan == "PCS" ? "selected" : "" }}>Pieces (PCS)</option>
                                                                            <option value="Botol" {{ $item->satuan == "Botol" ? "selected" : "" }}>Botol</option>
                                                                            <option value="Saset" {{ $item->satuan == "Saset" ? "selected" : "" }}>Saset</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            <div class="modal-footer bg-secondary">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success"
                                                                    name="submit">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            {{-- End Modal --}}
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Widgets End -->

            {{-- Start Modal --}}
            <form action="{{ route('manajemen-produk.store') }}" method="POST">
                @csrf
                <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
                    tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-secondary">
                                <h5 class="modal-title" id="staticBackdropLabel">Tambah Produk</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body bg-secondary">
                                <div class="row mb-3">
                                    <label for="namaProduk-tambah" class="col-sm-2 col-form-label">Nama Produk</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="namaProduk" class="form-control" id="namaProduk-tambah" placeholder="Masukkan Nama Produk"
                                            required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="kemasan-tambah" class="col-sm-2 col-form-label">Kemasan</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="kemasan" class="form-control" id="kemasan-tambah" placeholder="Contoh : 250 ml" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="jumlahStok-tambah" class="col-sm-2 col-form-label">Jumlah Stok</label>
                                    <div class="col-sm-10">
                                        <input type="number" step="0.01" name="jumlahStok" min="0" class="form-control" id="jumlahStok-tambah" placeholder="Masukkan Jumlah Stok (KG/L/PCS)"
                                            required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="satuan-tambah" class="col-sm-2 col-form-label">Satuan Produk</label>
                                    <div class="col-sm-10">
                                        <select class="form-select mb-3" aria-label="Default select example" name="satuan" id="satuan-tambah" required>
                                            <option selected disabled>Pilih Satuan Produk</option>
                                            <option value="KG">Kilogram (KG)</option>
                                            <option value="L">Liter (L)</option>
                                            <option value="PCS">Pieces (PCS)</option>
                                            <option value="Botol">Botol</option>
                                            <option value="Saset">Saset</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer bg-secondary">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" name="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {{-- End Modal --}}


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>
@endsection
