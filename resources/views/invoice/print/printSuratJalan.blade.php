<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Surat Jalan</title>
    <style>
        .header {
            height: 15cm;
            width: 25cm;
            margin: 0 auto;
        }

        .header .judul {
            margin-left: 30px;
            float: left;
            margin-right: 50px;
        }

        .header .judul h1 {
            font-size: 30px;
        }

        .header .judul table{
            margin-top: -20px;
            font-weight: bold;
        }

        .header .judul table tr {
            border: none;
        }

        .header .detailInvoice {
            padding-top: 55px;
            margin-bottom: -10px;
            font-weight: bold;
        }

        .header .detailInvoice table td,
        .detailInvoice table tr {
            border: none;
        }

        .content .listBarang h4 {
            text-align: center;
            margin-bottom: -2px;
        }

        .content .listBarang table {
            width: 845px;
            text-align: center;
            margin: 0 auto;
            font-weight: bold;
        }
        .footer table {
            width: 850px;
            text-align: center;
            margin: 5px auto;
            font-weight: bold;
        }
        .footer1{
            margin-bottom: -10px;
        }
        .footer1,
        .footer2 {
            margin-left: 30px;
            font-weight: bold;
        }
        .footer3 {
            text-align: center;
            font-style: italic;
            font-weight: bolder;
            padding-bottom: 10px;
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="judul">
            <h1>SINAR BERUNTUNG KALIMANTAN</h1>
            <table>
                <tr>
                    <td>JL. TRIKORA RAYA GUNTUNG MANGGIS</td>
                </tr>
                <tr>
                    <td>JL. AL-MUHAJIR RT.051/05 LANDASAN ULIN BANJARBARU</td>
                </tr>
                <tr>
                    <td>KALIMANTAN SELATAN</td>
                </tr>
                <tr>
                    <td>TELP : 0511-5912703</td>
                </tr>
            </table>
        </div>
        <div class="detailInvoice">
            <table>
                <tr>
                    <td>No. SJ</td>
                    <td>:</td>
                    <td>{{ $invoiceId }}</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{{ $invoices->tanggalPesanan }}</td>
                </tr>
                <tr>
                    <td>Pelanggan</td>
                    <td>:</td>
                    <td>{{ $member->namaToko }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{ $member->alamatToko }}</td>
                </tr>
                <tr>
                    <td>Telpon</td>
                    <td>:</td>
                    <td>{{ $member->nomorHP }}</td>
                </tr>
            </table>
        </div>
        <div class="content">
            <div class="listBarang">
                <h4>SURAT JALAN</h4>
                <table border="1">
                    <thead>
                        <td>No</td>
                        <td>Nama Produk</td>
                        <td>Kemasan</td>
                        <td>Jumlah (KG)</td>
                        <td>Jumlah (L)</td>
                        <td>Jumlah (PCS)</td>
                        <td>Jumlah Botol</td>
                        <td>Jumlah Saset</td>
                        <td>Keterangan</td>
                    </thead>
                    <tbody>
                        @php
                            $totalKg = 0;
                            $totalL = 0;
                            $totalPcs = 0;
                            $totalBotol = 0;
                            $totalSaset = 0;
                        @endphp
                        @foreach ($detailInvoices as $no => $produk)
                            <tr>
                                <td>{{ $no+1 }}</td>
                                <td style="width: 20%">{{ $produk->namaProduk }}</td>
                                <td style="width: 10%">{{ $produk->kemasan }}</td>
                                <td style="width: 10%">{{ $produk->satuan == "KG" ? $produk->jumlah." ".$produk->satuan : "-" }}</td>
                                <td style="width: 10%">{{ $produk->satuan == "L" ? $produk->jumlah." ".$produk->satuan : "-" }}</td>
                                <td style="width: 10%">{{ $produk->satuan == "PCS" ? $produk->jumlah." ".$produk->satuan : "-" }}</td>
                                <td style="width: 10%">{{ $produk->satuan == "Botol" ? $produk->jumlah." ".$produk->satuan : "-" }}</td>
                                <td style="width: 10%">{{ $produk->satuan == "Saset" ? $produk->jumlah." ".$produk->satuan : "-" }}</td>
                                <td style="width: 25%"></td>
                            </tr>
                            @php
                                if($produk->satuan == "KG"){
                                    $totalKg += $produk->jumlah;
                                }else if($produk->satuan == "L"){
                                    $totalL += $produk->jumlah;
                                }else if($produk->satuan == "PCS"){
                                    $totalPcs += $produk->jumlah;
                                }else if($produk->satuan == "Botol"){
                                    $totalBotol += $produk->jumlah;
                                }else{
                                    $totalSaset += $produk->jumlah;
                                }
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan="3">Jumlah</td>
                            <td>{{ $totalKg." KG" }}</td>
                            <td>{{ $totalL." L" }}</td>
                            <td>{{ $totalPcs." PCS" }}</td>
                            <td>{{ $totalBotol." Botol" }}</td>
                            <td>{{ $totalSaset." Saset" }}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footer">
            <table>
                <tr>
                    <td>Penerima</td>
                    <td>Kepala Gudang</td>
                    <td>Finance</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td>(.................................)</td>
                    <td>(.................................)</td>
                    <td>(.................................)</td>
                </tr>
            </table>
        </div>
    </div>
</body>

<script>
    window.print();
</script>

</html>