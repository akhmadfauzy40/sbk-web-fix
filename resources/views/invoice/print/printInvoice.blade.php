<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Invoice</title>
    <style>
        .header {
            height: 15cm;
            width: 25cm;
            margin: 0 auto;
        }

        .header .judul {
            margin-left: 30px;
            float: left;
            margin-right: 50px;
        }

        .header .judul h1 {
            font-size: 30px;
        }

        .header .judul table{
            margin-top: -20px;
            font-weight: bold;
        }

        .header .judul table tr {
            border: none;
        }

        .header .detailInvoice {
            padding-top: 55px;
            margin-bottom: -10px;
            font-weight: bold;
        }

        .header .detailInvoice table td,
        .detailInvoice table tr {
            border: none;
        }

        .content .listBarang h4 {
            text-align: center;
            margin-bottom: -2px;
        }

        .content .listBarang table {
            width: 845px;
            text-align: center;
            margin: 0 auto;
            font-weight: bold;
        }

        .footer table {
            width: 850px;
            text-align: center;
            margin: 5px auto;
            font-weight: bold;
        }

        .footer1{
            margin-bottom: -10px;
        }

        .footer1,
        .footer2 {
            margin-left: 30px;
            font-weight: bold;
        }

        .footer3 {
            text-align: center;
            font-style: italic;
            font-weight: bolder;
            padding-bottom: 10px;
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="judul">
            <h1>SINAR BERUNTUNG KALIMANTAN</h1>
            <table>
                <tr>
                    <td>JL. TRIKORA RAYA GUNTUNG MANGGIS</td>
                </tr>
                <tr>
                    <td>JL. AL-MUHAJIR RT.051/05 LANDASAN ULIN BANJARBARU</td>
                </tr>
                <tr>
                    <td>KALIMANTAN SELATAN</td>
                </tr>
                <tr>
                    <td>TELP : 0511-5912703</td>
                </tr>
            </table>
        </div>
        <div class="detailInvoice">
            <table>
                <tr>
                    <td>No. Invoice</td>
                    <td>:</td>
                    <td>{{ $invoiceId }}</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{{ \Carbon\Carbon::parse($invoices->tanggalPesanan)->translatedFormat('d F Y', 'id') }}</td>
                </tr>
                <tr>
                    <td>Pelanggan</td>
                    <td>:</td>
                    <td>{{ $member->namaToko }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{ $member->alamatToko }}</td>
                </tr>
            </table>
        </div>
        <div class="content">
            <div class="listBarang">
                <h4>INVOICE</h4>
                <table border="1">
                    <thead>
                        <td>No</td>
                        <td>Nama Produk</td>
                        <td>Kemasan</td>
                        <td>Jumlah</td>
                        <td>Harga</td>
                        <td>Total</td>
                    </thead>
                    <tbody>
                        @php
                            $totalBayar = 0;
                        @endphp
                        @foreach ($detailInvoices as $no => $produk)
                            <tr>
                                <td>{{ $no+1 }}</td>
                                <td style="text-align: left; padding-left:5px;">{{ $produk->namaProduk }}</td>
                                <td>{{ $produk->kemasan }}</td>
                                <td>{{ $produk->jumlah." ".$produk->satuan }}</td>
                                <td>{{ 'Rp.' . number_format($produk->harga, 0, ',', '.') }}</td>
                                <td>{{ 'Rp.' . number_format($produk->total, 0, ',', '.') }}</td>
                            </tr>
                            @php
                                $totalBayar += $produk->total;
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan="3">Total Pembayaran</td>
                            <td></td>
                            <td></td>
                            <td>{{ 'Rp.' . number_format($totalBayar, 0, ',', '.') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="footer">
            <table>
                <tr>
                    <td>Penerima</td>
                    <td>Finance</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>(.................................)</td>
                    <td>(.................................)</td>
                </tr>
            </table>
            <p class="footer1">
                Note : Pembayaran melalui Bank BRI No Rek : 1060-01-000-291-30-1 a/n : Wahyudianto
            </p>
            <p class="footer2">
                Tempo Pembayaran : .....
            </p>
            <p class="footer3">
                Barang yang sudah di beli tidak dapat di kembalikan
            </p>
        </div>
    </div>
</body>
<script>
    window.print();
</script>

</html>
