@extends('layout/layout')

@section('title')
    Detail Invoice - SBK-Web
@endsection

@section('sidebar')
    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="/welcome" class="navbar-brand mx-4 mb-3">
                <h3 class="text-primary"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;">&nbsp CV. SBK</h3>
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="{{ asset('img/person.png') }}" alt=""
                        style="width: 40px; height: 40px;">
                    <div
                        class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                    </div>
                </div>
                <div class="ms-3">
                    <h6 class="mb-0">{{ session('name') }}</h6>
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="/welcome" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                <a href="/manajemen-produk" class="nav-item nav-link" style="font-size: 14px;"><i class="fa fa-th me-2"></i>Manajemen Produk</a>
                <a href="/manajemen-stok" class="nav-item nav-link"><i class="fa fa-cubes me-2"></i>Manajemen Stok</a>
                <a href="/manajemen-customer" class="nav-item nav-link"><i class="fa fa-user me-2"></i>Membership</a>
                <a href="/invoice" class="nav-item nav-link active"><i class="fa fa-keyboard me-2"></i>Pesanan</a>
                <a href="/rekap" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Rekap</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->
@endsection


@section('konten')
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Sidebar Start -->

        <!-- Sidebar End -->


        <!-- Content Start -->
        <div class="content">
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="welcome" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><img src="{{ asset('img/logo2.png') }}" alt=""
                            style="height: 45px; width: 45px;"></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">

                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('img/person.png') }}" alt=""
                                style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">{{ session('name') }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                            <a href="/logout" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->


            <!-- Recent Sales Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary text-center rounded p-4">
                    <h1 class="display-6 mb-0">Detail Invoice {{ $invoiceId }}</h1>
                </div>
            </div>
            <!-- Recent Sales End -->


            <!-- Widgets Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="bg-secondary rounded h-100 p-4">
                            <div class="row mb-3">
                                <label for="id" class="col-sm-2 col-form-label">Nomor Invoice</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="id" name="id"
                                        value="{{ $invoiceId }}" readonly>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="namaMember" class="col-sm-2 col-form-label">Nama Member</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="namaMember" name="namaMember"
                                        value="{{ $member->namaMember }}" readonly>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="namaToko" class="col-sm-2 col-form-label">Nama Toko</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="namaToko" name="namaToko"
                                        value="{{ $member->namaToko }}" readonly>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="tanggal" name="tanggal"
                                        value="{{ $invoices->tanggalPesanan }}" readonly>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="alamatToko" class="col-sm-2 col-form-label">Alamat Toko</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="alamatToko" id="alamatToko" placeholder="Masukan Alamat Toko"
                                        style="height: 80px;" readonly>{{ $member->alamatToko }}</textarea>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="nomorHP" class="col-sm-2 col-form-label">Nomor HP</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomorHP" name="nomorHP"
                                        value="{{ $member->nomorHP }}" readonly>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-2 mt-2">List Produk yang di Pesan</h6>
                                <button type="button" class="btn btn-outline-success btn-sm m-2" data-bs-toggle="modal"
                                    data-bs-target="#tambahPesanan"
                                    {{ $invoices->statusPesanan === 'Selesai' ? 'disabled' : '' }}>Tambah Pesanan</button>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tbl_produk">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Produk</th>
                                            <th scope="col">Kemasan</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Total</th>
                                            <th scope="col" class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $totalBayar = 0;
                                        @endphp
                                        @foreach ($detailInvoices as $no => $produk)
                                            <tr>
                                                <th scope="row">{{ $no + 1 }}</th>
                                                <td>{{ $produk->namaProduk }}</td>
                                                <td>{{ $produk->kemasan }}</td>
                                                <td>{{ $produk->jumlah . ' ' . $produk->satuan }}</td>
                                                <td>{{ 'Rp.' . number_format($produk->harga, 0, ',', '.') }}</td>
                                                <td>{{ 'Rp.' . number_format($produk->total, 0, ',', '.') }}</td>
                                                <td class="text-center"><button type="button"
                                                        class="btn btn-outline-danger btn-sm m-2" data-bs-toggle="modal"
                                                        data-bs-target="#hapus{{ $no }}"
                                                        {{ $invoices->statusPesanan === 'Selesai' ? 'disabled' : '' }}><i
                                                            class="bi bi-trash"></i></button>
                                            </tr>
                                            @php
                                                $totalBayar += $produk->total;
                                            @endphp
                                            {{-- Modal Hapus Data --}}
                                            <div class="modal fade" id="hapus{{ $no }}" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <form action="{{ route('invoice-details.destroy', $produk->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-secondary">
                                                                <h5 class="modal-title text-primary"
                                                                    id="exampleModalLabel">Peringatan!!
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body bg-secondary">
                                                                Apakah Anda Yakin Ingin Menghapus Data Produk Ini dari List
                                                                Pesanan?
                                                            </div>
                                                            <div class="modal-footer bg-secondary">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" name="submit"
                                                                    class="btn btn-outline-primary m-2 btn-sm">Hapus</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            {{-- Modal Hapus Data --}}
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="mt-5">
                                    <h5>Total Yang Harus di Bayar : {{ 'Rp.' . number_format($totalBayar, 0, ',', '.') }}</h5>
                                </div>
                                <div class="mt-5 mb-2">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#hapusInvoice"
                                        {{ $detailInvoices->isEmpty() ?: 'disabled' }}>Batalkan Invoice</button>
                                    <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                        data-bs-target="#acc" {{ !$detailInvoices->isEmpty() ?: 'disabled' }}
                                        {{ $invoices->statusPesanan === 'Selesai' ? 'disabled' : '' }}>Selesaikan
                                        Pesanan</button>
                                    <a href="{{ route('invoice.print.printInvoice', base64_encode($invoiceId)) }}"
                                        target="blank"
                                        class="btn btn-success {{ $invoices->statusPesanan === 'Proses' ? 'disabled' : '' }}">Cetak
                                        Invoice</a>
                                    <a href="{{ route('invoice.print.printSuratJalan', base64_encode($invoiceId)) }}"
                                        target="blank"
                                        class="btn btn-success {{ $invoices->statusPesanan === 'Proses' ? 'disabled' : '' }}">Cetak
                                        Surat Jalan</a>
                                </div>
                                {{-- Modal Selesaikan Pesanan --}}
                                <div class="modal fade" id="acc" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <form action="{{ route('invoiceMember.update', base64_encode($invoiceId)) }}"
                                        method="post">
                                        @csrf
                                        @method('patch')
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-secondary">
                                                    <h5 class="modal-title" id="exampleModalLabel">Peringatan!!
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body bg-secondary">
                                                    Apakah Anda Yakin Ingin Menyelesaikan Pesanan ini? Setelah Anda Klik
                                                    'Ya' Invoice Tidak Akan Bisa Di Rubah
                                                    <input type="hidden" value="{{ $totalBayar }}" name="totalBayar">
                                                </div>
                                                <div class="modal-footer bg-secondary">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" name="submit"
                                                        class="btn btn-outline-success m-2 btn-sm">Ya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Modal Selesaikan Pesanan --}}
                                {{-- Modal Batalkan Pesanan --}}
                                <div class="modal fade" id="hapusInvoice" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <form action="{{ route('invoiceMember.destroy', base64_encode($invoiceId)) }}"
                                        method="post">
                                        @csrf
                                        @method('delete')
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-secondary">
                                                    <h5 class="modal-title" id="exampleModalLabel">Peringatan!!
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body bg-secondary">
                                                    Apakah Anda Yakin Ingin Menghapus Invoice Ini? Setelah Anda Klik
                                                    'Ya' Invoice Akan Di Hapus
                                                </div>
                                                <div class="modal-footer bg-secondary">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" name="submit"
                                                        class="btn btn-outline-primary m-2 btn-sm">Ya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- Modal Selesaikan Pesanan --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Widgets End -->

            {{-- Modal Tambah Data --}}
            <form action="{{ route('invoice-details.store') }}" method="POST">
                @csrf
                <div class="modal fade" id="tambahPesanan" data-bs-backdrop="static" data-bs-keyboard="false"
                    tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-secondary">
                                <h5 class="modal-title" id="staticBackdropLabel">Input Produk</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body bg-secondary">
                                <div class="row mb-3">
                                    <input type="text" class="form-control" id="id"
                                        name="id"value="{{ $invoiceId }}" readonly hidden>
                                    <label for="idProduk" class="col-sm-2 col-form-label">Produk</label>
                                    <div class="col-sm-10">
                                        <select class="form-select mb-3" aria-label="Default select example"
                                            name="idProduk" id="idProduk">
                                            <option selected disabled>Pilih Produk</option>
                                            @foreach ($products as $product)
                                                <option value="{{ $product->id }}"
                                                    data-stok="{{ $product->jumlahStok }}" data-satuan="{{ $product->satuan }}">
                                                    {{ $product->namaProduk }}&nbsp{{ $product->kemasan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="jumlah" class="col-sm-2 col-form-label">Masukkan Jumlah</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="jumlah" class="form-control" id="jumlah"
                                            placeholder="Masukan Jumlah Produk" min="1" max="" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="harga" class="col-sm-2 col-form-label">Masukkan Harga</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="harga" class="form-control" id="harga"
                                            placeholder="Masukan Harga Per-" min="1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer bg-secondary">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" name="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {{-- Modal Tambah Data --}}

            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>
@endsection
