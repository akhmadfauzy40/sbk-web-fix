@extends('layout/layout')

@section('title')
    Home - SBK-Web
@endsection

@section('sidebar')
    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">
            <a href="/welcome" class="navbar-brand mx-4 mb-3">
                <h3 class="text-primary"><img src="{{ asset('img/logo2.png') }}" alt=""
                        style="height: 45px; width: 45px;">&nbsp CV. SBK</h3>
            </a>
            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="position-relative">
                    <img class="rounded-circle" src="{{ asset('img/person.png') }}" alt=""
                        style="width: 40px; height: 40px;">
                    <div
                        class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                    </div>
                </div>
                <div class="ms-3">
                    <h6 class="mb-0">{{ session('name') }}</h6>
                    <span>Admin</span>
                </div>
            </div>
            <div class="navbar-nav w-100">
                <a href="/" class="nav-item nav-link active"><i class="fa fa-tachometer-alt me-2"></i>Home</a>
                <a href="/manajemen-produk" class="nav-item nav-link" style="font-size: 14px;"><i
                        class="fa fa-th me-2"></i>Manajemen Produk</a>
                <a href="/manajemen-stok" class="nav-item nav-link"><i class="fa fa-cubes me-2"></i>Manajemen Stok</a>
                <a href="/manajemen-customer" class="nav-item nav-link"><i class="fa fa-user me-2"></i>Membership</a>
                <a href="/invoice" class="nav-item nav-link"><i class="fa fa-keyboard me-2"></i>Pesanan</a>
                <a href="/rekap" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Rekap</a>
            </div>
        </nav>
    </div>
    <!-- Sidebar End -->
@endsection


@section('konten')
    <div class="container-fluid position-relative d-flex p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-dark position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->

        <!-- Content Start -->
        <div class="content">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <i class="fa fa-exclamation-circle me-2"></i>LOGIN BERHASIL!!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
                <a href="welcome" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><img src="{{ asset('img/logo2.png') }}" alt=""
                            style="height: 45px; width: 45px;"></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">

                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <img class="rounded-circle me-lg-2" src="{{ asset('img/person.png') }}" alt=""
                                style="width: 40px; height: 40px;">
                            <span class="d-none d-lg-inline-flex">{{ session('name') }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                            <a href="/logout" class="dropdown-item">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->

            <!-- Judul Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary text-center rounded p-4">
                    <h1 class="display-6 mb-0">Sistem Informasi CV. Sinar Beruntung Kalimantan</h1>
                </div>
            </div>
            <!-- Judul End -->

            {{-- content --}}

            <div class="container-fluid pt-4 px-4">
                <div class="row g-4">
                    <div class="col-sm-12 col-xl-6">
                        <div class="bg-secondary rounded p-4">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-0">Calender</h6>
                            </div>
                            <div id="calender"></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xl-6">
                        <div class="bg-secondary rounded h-100 p-4">
                            <div class="d-flex align-items-center justify-content-between mb-4">
                                <h6 class="mb-0">To Do List</h6>
                                <button type="button" class="btn btn-link m-2 btn-sm" data-bs-toggle="modal"
                                    data-bs-target="#staticBackdrop">
                                    Show All
                                </button>
                            </div>
                            <form action="{{ route('welcome.store') }}" method="post">
                                @csrf
                                <div class="d-flex mb-2">
                                    <input class="form-control bg-transparent" type="text" placeholder="Enter task"
                                        name="task" required>
                                    <button type="submit" name="submit" class="btn btn-primary ms-2">Add</button>
                                </div>
                            </form>
                            @forelse($toDoList as $index => $p)
                                @if ($index < 3)
                                    <div class="d-flex align-items-center border-bottom py-2">
                                        <input class="form-check-input m-0" type="checkbox">
                                        <div class="w-100 ms-3">
                                            <form action="{{ route('welcome.destroy', $p->id) }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <div class="d-flex w-100 align-items-center justify-content-between">
                                                    <span>{{ $p->task }}</span>
                                                    <button class="btn btn-sm" type="submit"><i
                                                            class="fa fa-times"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endif
                            @empty
                                <div class="d-flex align-items-center border-bottom py-2">
                                    <div class="w-100 ms-3">
                                        <div class="d-flex w-100 align-items-center justify-content-between">
                                            <span>Tidak ada task</span>
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>

            {{-- Start Modal --}}
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
                tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title" id="staticBackdropLabel">To Do List</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body bg-secondary">
                            @forelse($toDoList as $index => $p)
                                <div class="d-flex align-items-center border-bottom py-2">
                                    <input class="form-check-input m-0" type="checkbox">
                                    <div class="w-100 ms-3">
                                        <form action="{{ route('welcome.destroy', $p->id) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <div class="d-flex w-100 align-items-center justify-content-between">
                                                <span>{{ $p->task }}</span>
                                                <button class="btn btn-sm" type="submit"><i
                                                        class="fa fa-times"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @empty
                                <div class="d-flex align-items-center border-bottom py-2">
                                    <div class="w-100 ms-3">
                                        <div class="d-flex w-100 align-items-center justify-content-between">
                                            <span>Tidak ada task</span>
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                        <div class="modal-footer bg-secondary">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- End Modal --}}

            {{-- end content --}}


            <!-- Footer Start -->
            <div class="container-fluid pt-4 px-4">
                <div class="bg-secondary rounded-top p-4">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-center text-sm-start">
                            &copy; <a href="#">Your Site Name</a>, All Right Reserved.
                        </div>
                        <div class="col-12 col-sm-6 text-center text-sm-end">
                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a href="https://htmlcodex.com">HTML Codex</a>
                            <br>Distributed By: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End -->
        </div>
        <!-- Content End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>
@endsection
